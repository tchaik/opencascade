#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse
import os
import sys

SOURCE_DIR = "src"


def list_packages(packages_folder: str) -> int:
    """
    List source packages.
    """

    if not packages_folder:
        return 1

    packages_path = os.path.abspath(packages_folder)

    if not os.path.exists(packages_path):
        return 1
    elif not os.path.isdir(packages_path):
        return 1

    package_names = os.listdir(packages_path)

    for package_name in sorted(package_names):
        print(package_name)

    return 0


def generate_packages(packages_folder: str) -> int:
    """
    Generate a meson.build file for every source package.
    """

    if not packages_folder:
        return 1

    packages_path = os.path.abspath(packages_folder)

    if not os.path.exists(packages_path):
        return 1
    elif not os.path.isdir(packages_path):
        return 1

    package_names = os.listdir(packages_path)

    for package_name in sorted(package_names):
        files_file_path = os.path.join(packages_path, package_name, "FILES")

        if not os.path.exists(files_file_path):
            print(f"Package '{package_name}' does not have a FILES file.")
            continue

        package_headers = set()
        package_sources = set()

        with open(files_file_path, "r", encoding="utf-8") as files_file:
            for file_name in files_file:
                file_name = file_name.strip()
                if file_name.endswith(".hxx") or file_name.endswith(".lxx"):
                    package_headers.add(file_name)
                elif file_name.endswith(".cxx") or file_name.endswith(".c"):
                    package_sources.add(file_name)
                else:
                    print(f"Unknow file '{file_name}' in package '{package_name}'")

        build_file_path = os.path.join(packages_folder, package_name, "meson.build")

        if os.path.exists(build_file_path):
            os.remove(build_file_path)

        with open(build_file_path, "w", encoding="utf-8") as build_file:
            if package_headers:
                build_file.write("toolkit_headers += files([\n")

                for package_header in sorted(package_headers):
                    build_file.write(f"  '{package_header}',\n")

                build_file.write("])\n")
                build_file.write("\n")

            if package_sources:
                build_file.write("toolkit_sources += files([\n")

                for package_source in sorted(package_sources):
                    build_file.write(f"  '{package_source}',\n")

                build_file.write("])\n")

    return 0


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    sub_parsers = parser.add_subparsers(title="commands")

    sub_parser = sub_parsers.add_parser("ls-pkg",
                                        help="enumerate source packages")
    sub_parser.set_defaults(func=lambda options: setattr(options, "command", "ls-pkg"))

    sub_parser = sub_parsers.add_parser("gen-pkg",
                                        help="generate build scripts for source packages")
    sub_parser.set_defaults(func=lambda options: setattr(options, "command", "gen-pkg"))


    options = parser.parse_args()
    options.func(options)

    return_code = -1

    if not hasattr(options, "command"):
        sys.exit(return_code)

    if options.command == "ls-pkg":
        return_code = list_packages(SOURCE_DIR)
    elif options.command == "gen-pkg":
        return_code = generate_packages(SOURCE_DIR)

    sys.exit(return_code)


# ex:set ts=4 sw=4 et: